Voir les notes de cours dans [le wiki](https://gitlab.com/CoursDali/420-4p3-hu-d-veloppement-web-en-asp.net/s4-http-get-formulaires-http-post/cours/-/wikis/Accueil).


Les différentes méthodes d'action _[HttpPost]Saisie_ du contrôleur _RequetesController_ sont les méthodes d'action qui reçoivent les données du formulaire de différentes façons. **Vous devez utiliser une à la fois pour les tester.**
